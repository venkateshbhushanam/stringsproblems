
function string1(...str){
    //console.log(str[0])

    if(str[0]==0){
        return []
    }
    
    let numArray=[]
    for (let eachItem of str){
        let item=eachItem.split("$").join("")
       
        item=item.replace(/,/g,'')
        

        if(/^[-+]?\d*.?\d*$/.test(item)){
            //console.log(item)
            
            numArray.push(parseInt(item))
            
        }
        else{
            numArray.push(0)
        }
    }
   return numArray
}
    

module.exports=string1