function string4(str=""){
   

    let strValues=Object.values(str).join("")
    if(strValues===String(str) || str=="" || typeof(str)==="undefined"){
     return []
    } 
    
 
     if(str===undefined || Array.isArray(str)==true){
         return []
     }
           
     if( str=="" || str==[]|| typeof(str)=="string" || typeof(str)!="object" || Object.keys(str).length==0 ){
    
         
         return []
     } 
 
     let nameArray=[]
 
     for (each in str){
        
         let lower=str[each].toLowerCase()
         let cap=lower[0].toUpperCase()
         
         let titleCase=lower.replace(lower[0],cap)
         nameArray.push(titleCase)
       
     }
    
     return (nameArray.join(" "))
 
 }
 
 module.exports=string4
